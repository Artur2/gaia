﻿using System;
using System.Data.Entity;
using System.Threading;
using Gaia.Domain.Consumers;
using Gaia.Infrastructure.Configuration;
using Gaia.Infrastructure.Repositories;
using Gaia.Infrastructure.Repositories.UnitOfWork;
using Gaia.Infrastructure.Services;
using Gaia.Infrastructure.Services.Email;
using Gaia.Infrastructure.Services.SMS;
using MassTransit;
using Microsoft.Practices.Unity;

namespace Gaia.Agent
{
    class Program
    {
        private static IUnityContainer container;
        private static IBusControl bus;
        private const string MaintenanceQueue = "MaintenanceQueue";

        static void Main(string[] args)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<UnitOfWork, DatabaseConfiguration>());
            ConfigureContainer();
            bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost/"), hcfg =>
                {
                    hcfg.Heartbeat(60);
                    hcfg.Password("guest");
                    hcfg.Username("guest");
                });

                cfg.ReceiveEndpoint(host, MaintenanceQueue, ecfg =>
                {
                    ecfg.UseRetry(Retry.Interval(5, TimeSpan.FromSeconds(10)));
                    ecfg.UseConcurrencyLimit(1);

                    ecfg.Consumer(typeof(ActualizeBootstrapDataConsumer), type => container.Resolve(type));
                    ecfg.Consumer(typeof(EmailSendConsumer), type => container.Resolve(type));
                    ecfg.Consumer(typeof(SMSSendConsumer), type => container.Resolve(type));
                });
            });

            using (bus.Start())
            {
                while (true)
                {
                    Thread.Sleep(1000);
                }
            }
        }

        static void ConfigureContainer()
        {
            container = new UnityContainer();
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>));
            container.RegisterType<IQueryableUnitOfWork, UnitOfWork>(new PerResolveLifetimeManager());
            container.RegisterType<IHashService, HashService>(new TransientLifetimeManager());
            container.RegisterType<ActualizeBootstrapDataConsumer>();
            container.RegisterType<EmailSendConsumer>();
            container.RegisterType<SMSSendConsumer>();
            container.RegisterType<IEmailServiceFactory, EmailServiceFactory>();
            container.RegisterType<ISMSSenderServiceFactory, SMSSenderServiceFactory>();
        }
    }
}
