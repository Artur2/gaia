﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gaia.Domain.Entities.IdentityUserAgg;
using Gaia.Domain.Entities.PersonAgg;
using Gaia.Infrastructure.Repositories;
using Gaia.Infrastructure.Services;
using MassTransit;

namespace Gaia.Domain.Consumers
{
    [Serializable]
    public class ActualizeBootstrapData
    {

    }

    public class ActualizeBootstrapDataConsumer : IConsumer<ActualizeBootstrapData>
    {
        private IRepository<Person> _personsRepository;
        private IRepository<IdentityUser> _identityUsersRepository;
        private IRepository<IdentityRole> _identityRolesRepository;
        private IRepository<IdentityUserRole> _identityUserRolesRepository;
        private IHashService _hashService;

        public ActualizeBootstrapDataConsumer(
            IRepository<Person> personsRepository,
            IRepository<IdentityUser> identityUsersRepository,
            IRepository<IdentityRole> identityRolesRepository,
            IRepository<IdentityUserRole> identityUserRolesRepository,
            IHashService hashService)
        {
            _personsRepository = personsRepository;
            _identityUsersRepository = identityUsersRepository;
            _identityRolesRepository = identityRolesRepository;
            _identityUserRolesRepository = identityUserRolesRepository;
            _hashService = hashService;
        }

        public Task Consume(ConsumeContext<ActualizeBootstrapData> context)
        {
            var identityUser = new IdentityUser
            {
                UserName = "Gaia",
                PasswordHash = _hashService.Generate("Gaia_Pa$$w0rd"),
                Id = "960BEE85-9ABC-454F-B772-29FEB130C558"
            };

            if(_identityUsersRepository.Get(identityUser.Id) == null)
            {
                _identityUsersRepository.Add(identityUser);
            }

            var roles = new List<IdentityRole>
            {
                new IdentityRole {
                    Id = "665B5C9F-F840-4C41-BAEB-41CE7FD2D061",
                    Name = IdentityRole.System
                },
                new IdentityRole
                {
                    Id = "F1079ABD-37F1-42FC-913A-56418BE3A144",
                    Name = IdentityRole.Guest
                }
            };

            foreach (var role in roles)
            {
                if(_identityRolesRepository.Get(role.Id) == null)
                {
                    _identityRolesRepository.Add(role);
                }
            }

            var identityUserRoles = new List<IdentityUserRole>
            {
                new IdentityUserRole
                {
                    Id = "1D961E58-5C08-433E-8C70-CF010900EA9E",
                    RoleId = roles.First(item => item.Name == IdentityRole.System).Id,
                    UserId = identityUser.Id
                },
                new IdentityUserRole
                {
                    Id = "1D961E58-5C08-433E-8C70-CF010900EA9E",
                    RoleId = roles.First(item => item.Name == IdentityRole.Guest).Id,
                    UserId = identityUser.Id
                }
            };

            foreach (var identityUserRole in identityUserRoles)
            {
                if(_identityUserRolesRepository.Get(identityUserRole.Id) == null)
                {
                    _identityUserRolesRepository.Add(identityUserRole);
                }
            }

            var person = new Person
            {
                FirstName = "Gaia",
                Id = "6CD21DD6-1A51-4410-9144-94C114E36A7A",
                UserId = identityUser.Id
            };

            if(_personsRepository.Get(person.Id) == null)
            {
                _personsRepository.Add(person);
            }

            _personsRepository.UnitOfWork.Commit();

            return Task.FromResult(0);
        }
    }
}
