﻿using System.Threading.Tasks;
using Gaia.Infrastructure.Services.Email;
using MassTransit;

namespace Gaia.Domain.Consumers
{
    public class EmailSendData
    {
        public string To { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }

    public class EmailSendConsumer : IConsumer<EmailSendData>
    {
        private IEmailService _emailService;
        
        public EmailSendConsumer(IEmailServiceFactory factory)
        {
            _emailService = factory.Create(cfg =>
            {
                cfg.UseHost("[Your smtp host]", 587);
                cfg.UseCredentials("[Your account]", "[Your password]");
            });
        } 

        public async Task Consume(ConsumeContext<EmailSendData> context)
        {
            await _emailService.SendAsync(context.Message.To, context.Message.Subject, context.Message.Body);
        }
    }
}
