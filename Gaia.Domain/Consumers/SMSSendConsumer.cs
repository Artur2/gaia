﻿using System.Threading.Tasks;
using Gaia.Infrastructure.Services.SMS;
using MassTransit;

namespace Gaia.Domain.Consumers
{
    public class SMSSendData
    {
        public string From { get; set; }

        public string To { get; set; }

        public string Body { get; set; }
    }

    public class SMSSendConsumer : IConsumer<SMSSendData>
    {
        private ISMSSenderService _smsSenderService;

        public SMSSendConsumer(ISMSSenderServiceFactory factory)
        {
            _smsSenderService = factory.Create(cfg =>
            {
                cfg.UseCredentials("[Your AccountSID]", "[Your AuthToken]");
            });
        }

        public Task Consume(ConsumeContext<SMSSendData> context)
        {
            _smsSenderService.Send(context.Message.From, context.Message.To, context.Message.Body);

            return Task.FromResult(0);
        }
    }
}
