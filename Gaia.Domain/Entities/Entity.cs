﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gaia.Domain.Entities
{
    public abstract class Entity<TKey> : IEquatable<TKey>
    {
        [Key]
        public TKey Id { get; set; }

        public bool Equals(TKey other)
        {
#pragma warning disable
            return object.Equals(Id, other);
#pragma warning restore
        }
    }
}
