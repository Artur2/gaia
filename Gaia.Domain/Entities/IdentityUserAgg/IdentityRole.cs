﻿using System.Collections.Generic;
using Gaia.Infrastructure.Configuration;
using Microsoft.AspNet.Identity;

namespace Gaia.Domain.Entities.IdentityUserAgg
{
    [Entity]
    public class IdentityRole : Entity<string>, IRole<string>
    {
        public string Name { get; set; }

        public virtual ICollection<IdentityUserRole> Users { get; set; }

        public const string System = "System";

        public const string Guest = "Guest";
    }
}
