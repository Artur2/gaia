﻿using System;
using System.Collections.Generic;
using Gaia.Infrastructure.Configuration;
using Microsoft.AspNet.Identity;

namespace Gaia.Domain.Entities.IdentityUserAgg
{
    [Entity]
    public class IdentityUser : Entity<string>, IUser<string>
    {
        public string UserName { get; set; }

        public virtual ICollection<IdentityUserRole> Roles { get; set; }

        public string PasswordHash { get; set; }

        public DateTime? LockoutEndDate { get; set; }

        public int AccessFailedCount { get; set; }

        public bool LockoutEnabled { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string EmailConfirmationToken { get; set; }

        public string Phone { get; set; }

        public bool PhoneConfirmed { get; set; }
    }
}
