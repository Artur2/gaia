﻿using System.ComponentModel.DataAnnotations;
using Gaia.Infrastructure.Configuration;

namespace Gaia.Domain.Entities.IdentityUserAgg
{
    [Entity]
    public class IdentityUserRole : Entity<string>
    {
        [Required]
        public string UserId { get; set; }

        public virtual IdentityUser User { get; set; }

        [Required]
        public string RoleId { get; set; }

        public virtual IdentityRole Role { get; set; }
    }
}
