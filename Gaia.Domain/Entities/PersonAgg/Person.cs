﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gaia.Domain.Entities.IdentityUserAgg;
using Gaia.Infrastructure.Configuration;

namespace Gaia.Domain.Entities.PersonAgg
{
    [Entity]
    public class Person : Entity<string>
    {
        [Required]
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string LastName { get; set; }

        public string ParentId { get; set; }

        public virtual Person Parent { get; set; }

        public virtual ICollection<Person> Children { get; set; }

        [Required]
        public string UserId { get; set; }

        public virtual IdentityUser User { get; set; }
    }
}
