﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Gaia.Domain.Entities.IdentityUserAgg;
using Gaia.Infrastructure.Repositories;
using Microsoft.AspNet.Identity;

namespace Gaia.Domain.Stores
{
    public class RoleStore : IQueryableRoleStore<IdentityRole>
    {
        private IRepository<IdentityRole> _rolesRepository;

        public RoleStore(IRepository<IdentityRole> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        public IQueryable<IdentityRole> Roles => _rolesRepository.AsQueryable();

        public Task CreateAsync(IdentityRole role)
        {
            role.Id = Guid.NewGuid().ToString();
            _rolesRepository.Add(role);
            _rolesRepository.UnitOfWork.Commit();

            return Task.FromResult(0);
        }

        public Task DeleteAsync(IdentityRole role)
        {
            _rolesRepository.Remove(role.Id);
            _rolesRepository.UnitOfWork.Commit();

            return Task.FromResult(0);
        }

        public void Dispose()
        {
            
        }

        public Task<IdentityRole> FindByIdAsync(string roleId)
        {
            return Task.FromResult(_rolesRepository.Get(roleId));
        }

        public Task<IdentityRole> FindByNameAsync(string roleName)
        {
            return Task.FromResult(Roles.FirstOrDefault(item => item.Name == roleName));
        }

        public Task UpdateAsync(IdentityRole role)
        {
            //TODO:
            return Task.FromResult(0);
        }
    }
}
