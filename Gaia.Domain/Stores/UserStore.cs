﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gaia.Domain.Entities.IdentityUserAgg;
using Gaia.Infrastructure.Repositories;
using Microsoft.AspNet.Identity;

namespace Gaia.Domain.Stores
{
    public class UserStore : IUserStore<IdentityUser>,
        IUserRoleStore<IdentityUser>,
        IQueryableUserStore<IdentityUser>,
        IUserPasswordStore<IdentityUser>,
        IUserLockoutStore<IdentityUser,string>,
        IUserTwoFactorStore<IdentityUser,string>,
        IUserEmailStore<IdentityUser>,
        IUserPhoneNumberStore<IdentityUser>,
        IUserTokenProvider<IdentityUser,string>
    {
        private const string EmailConfirmationTokenPurpose = "Confirmation";

        private IRepository<IdentityUser> _usersRepository;
        private IRepository<IdentityRole> _rolesRepository;
        private IRepository<IdentityUserRole> _userRolesRepository;

        public UserStore(
            IRepository<IdentityUser> usersRepository,
            IRepository<IdentityRole> rolesRepository,
            IRepository<IdentityUserRole> userRolesRepository)
        {
            _usersRepository = usersRepository;
            _rolesRepository = rolesRepository;
            _userRolesRepository = userRolesRepository;
        }

        public IQueryable<IdentityUser> Users => _usersRepository.AsQueryable();

        public Task CreateAsync(IdentityUser user)
        {
            user.Id = Guid.NewGuid().ToString();
            _usersRepository.Add(user);
            _usersRepository.UnitOfWork.Commit();

            return Task.FromResult(0);
        }

        public Task UpdateAsync(IdentityUser user)
        {
            _usersRepository.UnitOfWork.Commit();
            return Task.FromResult(0);
        }

        public Task DeleteAsync(IdentityUser user)
        {
            _usersRepository.Remove(user.Id);
            _usersRepository.UnitOfWork.Commit();

            return Task.FromResult(0);
        }

        public Task<IdentityUser> FindByIdAsync(string userId)
        {
            return Task.FromResult(_usersRepository.Get(userId));
        }

        public Task<IdentityUser> FindByNameAsync(string userName)
        {
            return Task.FromResult(Users.FirstOrDefault(item => item.UserName == userName));
        }

        public void Dispose()
        {
            //
        }

        public Task AddToRoleAsync(IdentityUser user, string roleName)
        {
            var role = _rolesRepository.AsQueryable().FirstOrDefault(item => item.Name == roleName);
            if (role == null)
                throw new NullReferenceException(nameof(role));

            var userRole = new IdentityUserRole
            {
                Role = role,
                RoleId = role.Id,
                User = user,
                UserId = user.Id
            };

            _userRolesRepository.Add(userRole);
            _userRolesRepository.UnitOfWork.Commit();

            return Task.FromResult(0);
        }

        public Task RemoveFromRoleAsync(IdentityUser user, string roleName)
        {
            var role = _rolesRepository.AsQueryable().FirstOrDefault(item => item.Name == roleName);
            if (role == null)
                throw new NullReferenceException(nameof(role));
            var userRole = _userRolesRepository.AsQueryable().FirstOrDefault(item => item.RoleId == role.Id && item.UserId == user.Id);
            if (userRole == null)
                throw new NullReferenceException(nameof(userRole));

            _userRolesRepository.Remove(userRole.Id);
            _userRolesRepository.UnitOfWork.Commit();

            return Task.FromResult(0);
        }

        public Task<IList<string>> GetRolesAsync(IdentityUser user)
        {
            var names = from userRole in _userRolesRepository.AsQueryable()
                        where userRole.UserId == user.Id
                        join role in _rolesRepository.AsQueryable() on userRole.RoleId equals role.Id
                        select role.Name;

            return Task.FromResult((IList<string>)names.ToList());
        }

        public Task<bool> IsInRoleAsync(IdentityUser user, string roleName)
        {
            var roles = from userRole in _userRolesRepository.AsQueryable()
                        where userRole.UserId == user.Id
                        join role in _rolesRepository.AsQueryable() on userRole.RoleId equals role.Id
                        where role.Name == roleName
                        select role.Id;
            return Task.FromResult(roles.Any());
        }

        public Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(IdentityUser user)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(IdentityUser user)
        {
            return Task.FromResult(user.LockoutEndDate.HasValue
                    ? new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDate.Value, DateTimeKind.Utc))
                    : new DateTimeOffset());
        }

        public Task SetLockoutEndDateAsync(IdentityUser user, DateTimeOffset lockoutEnd)
        {
            user.LockoutEndDate = lockoutEnd == DateTimeOffset.MinValue ? (DateTime?)null : lockoutEnd.UtcDateTime;
            return Task.FromResult(0);
        }

        public Task<int> IncrementAccessFailedCountAsync(IdentityUser user)
        {
            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task ResetAccessFailedCountAsync(IdentityUser user)
        {
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        public Task<int> GetAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> GetLockoutEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(user.LockoutEnabled);
        }

        public Task SetLockoutEnabledAsync(IdentityUser user, bool enabled)
        {
            user.LockoutEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task SetTwoFactorEnabledAsync(IdentityUser user, bool enabled)
        {
            user.TwoFactorEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task<bool> GetTwoFactorEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(user.TwoFactorEnabled);
        }

        public Task SetEmailAsync(IdentityUser user, string email)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public Task<string> GetEmailAsync(IdentityUser user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(IdentityUser user)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(IdentityUser user, bool confirmed)
        {
            user.EmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task<IdentityUser> FindByEmailAsync(string email)
        {
            return Task.FromResult(_usersRepository.AsQueryable().FirstOrDefault(user => user.Email == email));
        }

        public Task SetPhoneNumberAsync(IdentityUser user, string phoneNumber)
        {
            user.Phone = phoneNumber;
            return Task.FromResult(0);
        }

        public Task<string> GetPhoneNumberAsync(IdentityUser user)
        {
            return Task.FromResult(user.Phone);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(IdentityUser user)
        {
            return Task.FromResult(user.PhoneConfirmed);
        }

        public Task SetPhoneNumberConfirmedAsync(IdentityUser user, bool confirmed)
        {
            user.PhoneConfirmed = confirmed;
            return Task.FromResult(0);
        }

        private Dictionary<string, Func<string>> TokenGenerators = new Dictionary<string, Func<string>>
        {
            { EmailConfirmationTokenPurpose, () => Guid.NewGuid().ToString() }
        };

        public Task<string> GenerateAsync(string purpose, UserManager<IdentityUser, string> manager, IdentityUser user)
        {
            if (!TokenGenerators.ContainsKey(purpose))
                throw new NotSupportedException();

            return Task.FromResult(TokenGenerators[purpose]());
        }

        private Dictionary<string, Func<IdentityUser, string, bool>> TokenValidators = new Dictionary<string, Func<IdentityUser, string, bool>>
        {
            { EmailConfirmationTokenPurpose, (user,token) => user.EmailConfirmationToken ==  token }
        };

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<IdentityUser, string> manager, IdentityUser user)
        {
            if (!TokenValidators.ContainsKey(purpose))
                throw new NotSupportedException();

            return Task.FromResult(TokenValidators[purpose](user, token));
        }

        public Task NotifyAsync(string token, UserManager<IdentityUser, string> manager, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsValidProviderForUserAsync(UserManager<IdentityUser, string> manager, IdentityUser user)
        {
            throw new NotImplementedException();
        }
    }
}
