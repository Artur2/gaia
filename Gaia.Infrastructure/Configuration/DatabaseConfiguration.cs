﻿using System.Data.Entity.Migrations;
using Gaia.Infrastructure.Repositories.UnitOfWork;

namespace Gaia.Infrastructure.Configuration
{
    public class DatabaseConfiguration : DbMigrationsConfiguration<UnitOfWork>
    {
        public DatabaseConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }
    }
}
