﻿using System;

namespace Gaia.Infrastructure.Configuration
{
    /// <summary>
    /// Marks class as entity, which registered at model configuration
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityAttribute : Attribute
    {
    }
}
