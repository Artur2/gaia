﻿using System.Linq;
using Gaia.Infrastructure.Repositories.UnitOfWork;

namespace Gaia.Infrastructure.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(object id);

        void Add(TEntity entity);

        void Remove(object id);

        IQueryable<TEntity> AsQueryable();

        IUnitOfWork UnitOfWork { get; }
    }
}
