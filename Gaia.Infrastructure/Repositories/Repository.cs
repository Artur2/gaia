﻿using System;
using System.Linq;
using Gaia.Infrastructure.Repositories.UnitOfWork;

namespace Gaia.Infrastructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private IQueryableUnitOfWork _unitOfWork;

        public Repository(IQueryableUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork => _unitOfWork;

        public void Add(TEntity entity)
        {
            _unitOfWork.GetSet<TEntity>().Add(entity);
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return _unitOfWork.GetSet<TEntity>();
        }

        public TEntity Get(object id)
        {
            return _unitOfWork.GetSet<TEntity>().Find(id);
        }

        public void Remove(object id)
        {
            var entity = _unitOfWork.GetSet<TEntity>().Find(id);
            if (entity == null)
                throw new NullReferenceException($"item with key {id} not found");

            _unitOfWork.GetSet<TEntity>().Remove(entity);
        }
    }
}
