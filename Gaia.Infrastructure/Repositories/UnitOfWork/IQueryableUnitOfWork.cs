﻿using System.Data.Entity;

namespace Gaia.Infrastructure.Repositories.UnitOfWork
{
    public interface IQueryableUnitOfWork : IUnitOfWork
    {
        void Attach<TEntity>(TEntity entity) where TEntity : class;

        IDbSet<TEntity> GetSet<TEntity>() where TEntity : class;
    }
}
