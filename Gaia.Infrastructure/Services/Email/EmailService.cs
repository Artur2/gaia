﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Gaia.Infrastructure.Services.Email
{
    public class EmailService : IEmailService
    {
        private EmailServiceConfiguration _configuration;

        public EmailService(EmailServiceConfiguration configuration)
        {
            _configuration = configuration;
        }

        private SmtpClient ConfigureClient()
        {
            var client = new SmtpClient(_configuration.HostUrl, _configuration.Port);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(_configuration.UserName, _configuration.Password);

            return client;
        }

        public async Task SendAsync(string to, string title, string body)
        {
            var message = new MailMessage(_configuration.UserName, to, title, body);

            var client = ConfigureClient();
            await client.SendMailAsync(message);
        }

        public void Send(string to, string title, string body)
        {
            var message = new MailMessage(_configuration.UserName, to, title, body);

            var client = ConfigureClient();
            client.Send(message);
        }
    }
}
