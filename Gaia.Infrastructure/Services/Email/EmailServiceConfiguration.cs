﻿namespace Gaia.Infrastructure.Services.Email
{
    public class EmailServiceConfiguration
    {
        internal string Password { get; set; }

        internal string UserName { get; set; }

        internal string HostUrl { get; set; }

        internal int Port { get; set; }
    }
}
