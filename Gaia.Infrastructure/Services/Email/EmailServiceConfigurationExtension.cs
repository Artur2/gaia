﻿namespace Gaia.Infrastructure.Services.Email
{
    public static class EmailServiceConfigurationExtension
    {
        public static void UseCredentials(this EmailServiceConfiguration @this, string userName, string password)
        {
            @this.UserName = userName;
            @this.Password = password;
        }

        public static void UseHost(this EmailServiceConfiguration @this, string hostUrl, int port)
        {
            @this.HostUrl = hostUrl;
            @this.Port = port;
        }
    }
}
