﻿using System;

namespace Gaia.Infrastructure.Services.Email
{
    public class EmailServiceFactory : IEmailServiceFactory
    {
        public IEmailService Create(Action<EmailServiceConfiguration> configure)
        {
            var configuration = new EmailServiceConfiguration();
            configure(configuration);
            return new EmailService(configuration);
        }
    }
}
