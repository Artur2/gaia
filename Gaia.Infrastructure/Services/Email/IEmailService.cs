﻿using System.Threading.Tasks;

namespace Gaia.Infrastructure.Services.Email
{
    public interface IEmailService
    {
        Task SendAsync(string to, string title, string body);

        void Send(string to, string title, string body);
    }
}
