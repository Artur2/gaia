﻿using System;

namespace Gaia.Infrastructure.Services.Email
{
    public interface IEmailServiceFactory
    {
        IEmailService Create(Action<EmailServiceConfiguration> configure);
    }
}
