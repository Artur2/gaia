﻿namespace Gaia.Infrastructure.Services
{
    public interface IHashService
    {
        string Generate(string value);
    }
}
