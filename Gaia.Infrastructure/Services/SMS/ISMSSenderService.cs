﻿namespace Gaia.Infrastructure.Services.SMS
{
    public interface ISMSSenderService
    {
        void Send(string from, string to, string body);
    }
}
