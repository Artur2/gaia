﻿using System;

namespace Gaia.Infrastructure.Services.SMS
{
    public interface ISMSSenderServiceFactory
    {
        ISMSSenderService Create(Action<SMSSenderServiceConfiguration> configure);
    }
}
