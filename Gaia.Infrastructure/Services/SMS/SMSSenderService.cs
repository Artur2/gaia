﻿using Twilio;

namespace Gaia.Infrastructure.Services.SMS
{
    public class SMSSenderService : ISMSSenderService
    {
        private SMSSenderServiceConfiguration _configuration;

        public SMSSenderService(SMSSenderServiceConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Send(string from, string to, string body)
        {
            var client = new TwilioRestClient(_configuration.UserName, _configuration.Password);

            client.SendMessage(from, to, body);
        }
    }
}
