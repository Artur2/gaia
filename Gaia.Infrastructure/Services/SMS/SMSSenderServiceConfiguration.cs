﻿namespace Gaia.Infrastructure.Services.SMS
{
    public class SMSSenderServiceConfiguration
    {
        internal string UserName { get; set; }

        internal string Password { get; set; }
    }
}
