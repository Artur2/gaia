﻿namespace Gaia.Infrastructure.Services.SMS
{
    public static class SMSSenderServiceConfigurationExtensions
    {
        public static void UseCredentials(this SMSSenderServiceConfiguration @this, string userName, string password)
        {
            @this.UserName = userName;
            @this.Password = password;
        }
    }
}
