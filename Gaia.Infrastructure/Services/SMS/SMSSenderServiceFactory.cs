﻿using System;

namespace Gaia.Infrastructure.Services.SMS
{
    public class SMSSenderServiceFactory : ISMSSenderServiceFactory
    {
        public ISMSSenderService Create(Action<SMSSenderServiceConfiguration> configure)
        {
            var configuration = new SMSSenderServiceConfiguration();
            configure(configuration);

            return new SMSSenderService(configuration);
        }
    }
}
