﻿using System.ServiceProcess;

namespace Gaia.Supervisor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SupervisorService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
