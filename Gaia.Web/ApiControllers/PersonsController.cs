﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Gaia.Domain.Entities.PersonAgg;
using Gaia.Infrastructure.Repositories;
using Gaia.Web.DTO_s;

namespace Gaia.Web.ApiControllers
{
    public class PersonsController : ApiController
    {
        private IRepository<Person> _personRepository;

        public PersonsController(IRepository<Person> personRepository)
        {
            _personRepository = personRepository;
        }

        // GET: api/Persons
        public IEnumerable<PersonDTO> Get() => _personRepository.AsQueryable()
                .Select(person => Mapper.Map<PersonDTO>(person));

        // GET: api/Persons/5
        public PersonDTO Get(string id) => Mapper.Map<PersonDTO>(_personRepository.Get(id));

        // POST: api/Persons
        public void Post([FromBody]PersonDTO value)
        {
            var person = Mapper.Map<Person>(value);
            person.Id = Guid.NewGuid().ToString();
            _personRepository.Add(person);
            _personRepository.UnitOfWork.Commit();
        }

        // DELETE: api/Persons/5
        public void Delete(string id)
        {
            _personRepository.Remove(id);
            _personRepository.UnitOfWork.Commit();
        }
    }
}
