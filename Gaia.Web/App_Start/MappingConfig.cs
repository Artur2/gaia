﻿using AutoMapper;
using Gaia.Web.DTO_s;

namespace Gaia.Web.App_Start
{
    public static class MappingConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<MappingProfile>());
        }
    }
}