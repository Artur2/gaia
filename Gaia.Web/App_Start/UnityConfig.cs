using System;
using Gaia.Domain.Entities.IdentityUserAgg;
using Gaia.Domain.Stores;
using Gaia.Infrastructure.Repositories;
using Gaia.Infrastructure.Repositories.UnitOfWork;
using MassTransit;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;

namespace Gaia.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        private static IBusControl _busControl;
        private static object _lock = new object();

        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            container.RegisterType<IUserStore<IdentityUser>, UserStore>();
            container.RegisterType<IUnitOfWork, UnitOfWork>(new PerRequestLifetimeManager());
            container.RegisterType<IQueryableUnitOfWork, UnitOfWork>(new PerResolveLifetimeManager());
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new PerResolveLifetimeManager());

            container.RegisterInstance(MvcApplication.BusControl, new ExternallyControlledLifetimeManager());
        }
    }
}
