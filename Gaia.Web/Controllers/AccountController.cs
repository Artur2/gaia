﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Gaia.Domain.Entities.IdentityUserAgg;
using Gaia.Web.App_Start;
using Gaia.Web.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Gaia.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication; 

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var authenticatedUser = await UserManager.FindByEmailAsync(model.UserName);
            if(!await UserManager.IsEmailConfirmedAsync(authenticatedUser.Id))
            {
                ModelState.AddModelError("", "Please confirm email");

                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, isPersistent: true, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return Redirect(model.ReturnUrl ?? "/");
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return Redirect("/");
        }

        public async Task<ActionResult> ConfirmRegistration(string userId, string token)
        {
            var authenticatedUser = await UserManager.FindByIdAsync(userId);
            if (authenticatedUser == null)
                return HttpNotFound();

            var result = await UserManager.ConfirmEmailAsync(userId, token);
            if (!result.Succeeded)
                return HttpNotFound();

            //TODO: add confirmation notification page
            return Redirect("/");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var identityUser = new IdentityUser
            {
                UserName = model.UserName,
                Email = model.Email
            };

            identityUser.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
            await UserManager.CreateAsync(identityUser);

            identityUser.EmailConfirmationToken = await UserManager.GenerateEmailConfirmationTokenAsync(identityUser.Id);
            
            //TODO: add notification by email with url to confirm
            //TODO: add redirect to email sended notification page
            return Redirect("/");
        }
    }
}