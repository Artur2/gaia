﻿using System.Web.Mvc;
using Gaia.Domain.Entities.PersonAgg;
using Gaia.Infrastructure.Repositories;
using MassTransit;

namespace Gaia.Web.Controllers
{
    public class HomeController : Controller
    {
        private IBusControl busControl;
        private IRepository<Person> personsRepository;

        public HomeController(IBusControl busControl, IRepository<Person> personsRepository)
        {
            this.busControl = busControl;
            this.personsRepository = personsRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}