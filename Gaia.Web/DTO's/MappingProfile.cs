﻿using AutoMapper;
using Gaia.Domain.Entities.PersonAgg;

namespace Gaia.Web.DTO_s
{
    public class MappingProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "DefaultProfile";
            }
        }

        protected override void Configure()
        {
            this.CreateMap<PersonDTO, Person>();
            this.CreateMap<Person, PersonDTO>();
        }
    }
}