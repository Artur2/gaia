﻿namespace Gaia.Web.DTO_s
{
    public class PersonDTO
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string LastName { get; set; }
    }
}