﻿using System;
using System.Data.Entity;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Gaia.Infrastructure.Configuration;
using Gaia.Infrastructure.Repositories.UnitOfWork;
using Gaia.Web.App_Start;
using MassTransit;

namespace Gaia.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IBusControl _busControl;
        private static BusHandle _busHandle; 

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MappingConfig.Configure();
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<UnitOfWork, DatabaseConfiguration>());
        }

        public static IBusControl BusControl => _busControl ?? (_busControl = ConfigureBus());

        protected void Application_End()
        {
            if (_busHandle != null)
            {
                _busHandle.Stop(TimeSpan.FromSeconds(30));
                _busHandle = null;
            }
            if (_busControl != null)
            {
                _busControl.Stop();
                _busControl = null;
            }
        }
        
        protected static IBusControl ConfigureBus()
        {
            //TODO: Investigate how to close properly bus
            var bus = Bus.Factory.CreateUsingRabbitMq(busConfiguration =>
            {
                busConfiguration.UseJsonSerializer();
                busConfiguration.Host(new Uri("rabbitmq://localhost/"), cfg =>
                {
                    cfg.Heartbeat(60);
                    cfg.Password("guest");
                    cfg.Username("guest");
                });
            });
            _busHandle = bus.Start();

            return bus;
        }
    }
}
