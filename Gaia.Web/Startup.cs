﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Gaia.Web.Startup))]
namespace Gaia.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}