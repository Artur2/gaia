﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Gaia.Domain.Entities.IdentityUserAgg;
using Gaia.Infrastructure.Repositories;

namespace Gaia.Web.ViewModels
{
    public class RegisterViewModel : IValidatableObject
    {
        [Required]
        public string UserName { get; set; }

        [Compare("PasswordConfirmation")]
        [Required]
        public string Password { get; set; }

        [Required]
        public string PasswordConfirmation { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Phone]
        public string Phone { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var usersRepository = DependencyResolver.Current.GetService<IRepository<IdentityUser>>();

            if (!string.IsNullOrWhiteSpace(Email) && usersRepository.AsQueryable().Any(user => user.Email == Email))
                yield return new ValidationResult("Email already used", new[] { nameof(Email) });
        }
    }
}