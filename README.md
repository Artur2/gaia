# Gaia #

Project representing Greek gods genealogy, with registration.

* Support Two Factor Authorization
* Support Email Verification
* Support Message Queue for consumers(Email, SMS, Bootstrap data actualization)

### Used Technologies ###

* ASP.NET MVC
* Entity Framework 6.1
* Unity DI
* RabbitMQ
* MassTransit
* AutoMapper
* Windows Service as Supervisor

### What project has at this time ###

* Agents with consumers: Email send, SMS send
* Actualization of bootstrap data with parent of all gods - Gaia
* Authorization, meanwhile without registration
* Authentication by username

### What in plans ###

* Registration
* Authorization by SMS
* CQRS + ES using MongoDB as read storage, which will update through MQ 
* Generation of genealogy tree for each of god